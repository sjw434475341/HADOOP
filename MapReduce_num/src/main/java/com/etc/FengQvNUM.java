package com.etc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

import java.util.HashMap;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 10:33
 * @Description:
 */
public class FengQvNUM extends Partitioner<Text,REwritable> {
    static HashMap<String, Integer> provinceMap = new HashMap<>();
    static {
        provinceMap.put("134",0);
        provinceMap.put("135",1);
        provinceMap.put("136",2);
        provinceMap.put("137",3);
        provinceMap.put("138",4);
        provinceMap.put("139",5);
        provinceMap.put("150",6);
        provinceMap.put("159",7);
        provinceMap.put("182",8);
        provinceMap.put("182",9);

    }

    /**
     * k v 对应map的输出
     * @param key
     * @param bean
     * @param numPartitions
     * @return
     */
    @Override
    public int getPartition(Text key, REwritable bean, int numPartitions) {
            Integer code = provinceMap.get(key.toString().substring(0,3));
            return code == null ? 10:code;
    }
}
