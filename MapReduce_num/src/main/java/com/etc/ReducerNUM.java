package com.etc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @Auther: Administrator
 * @Date: 2018/9/3 08:37
 * @Description:
 */
public class ReducerNUM extends Reducer <Text,REwritable,Text,REwritable> {
    List<REwritable> list =new ArrayList<>();

    @Override
    protected void reduce(Text key, Iterable<REwritable> values, Context context) throws IOException, InterruptedException {
        REwritable result = new REwritable();
        int a = 0,b=0;
           for (REwritable value : values){
               a+=value.getUpFlow();
               b+=value.getDownFlow();
           }

           result.setPhonenum(key.toString());
           result.setUpFlow(a);
           result.setDownFlow(b);
           result.setSumFlow();
           list.add(result);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        Collections.sort(list);
        for (REwritable num : list){
            context.write(new Text(num.getPhonenum()),num);
        }
    }
}
