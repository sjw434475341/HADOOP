package com.etc;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/8/31 18:23
 * @Description:
 */
public class MapperNUM extends Mapper<LongWritable,Text,Text,REwritable> {

    REwritable rw = new REwritable();
    Text t = new Text();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //获取第一行
      String str = value.toString();
      //截取
      String[] line = str.split("\t");
      //封装对象
      String num = line[1];
      long upflow =Long.parseLong(line[line.length-3]);
        long dflow = Long.parseLong(line[line.length-2]);

       rw.set(upflow,dflow);
       t.set(num);
      //REwritable rewritable = new REwritable(upflow,dflow);
      context.write(t, rw);
    }
}
