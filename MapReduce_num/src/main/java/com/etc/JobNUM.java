package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 09:01
 * @Description:
 */
public class JobNUM {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
       //获取对象信息，或job对象实例
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
      //指定本程序的jar包所在的本地路径
        job.setJarByClass(JobNUM.class);
      //指定本业务job要使用的mapper/reduceer业务类
        job.setMapperClass(MapperNUM.class);
        job.setReducerClass(ReducerNUM.class);
      //指定mapper输出数据的kv类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(REwritable.class);
      //指定最终输出数据的kv类型
        job.setOutputKeyClass(REwritable.class);
        job.setOutputValueClass(Text.class);
       // job.setPartitionerClass(FengQvNUM.class);
        //job.setNumReduceTasks(10);

        File output = new File("f:\\output");
        if (output.exists()){
            FileUtil.fullyDelete(output);
        }
      //指定job的输入原始目录所在
        FileInputFormat.setInputPaths(job , new Path("f:\\input"));
        FileOutputFormat.setOutputPath(job , new Path("f:\\output"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
