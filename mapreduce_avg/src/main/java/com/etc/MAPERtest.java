package com.etc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/8/31 14:53
 * @Description:
 */
public class MAPERtest extends Mapper<LongWritable,Text,Text,IntWritable> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String str = value.toString();
        String[] split = str.split(",");
        context.write(new Text(split[0]),new IntWritable(Integer.parseInt(split[1])));
    }
}
