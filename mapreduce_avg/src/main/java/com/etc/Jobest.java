package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import java.io.File;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/8/31 15:07
 * @Description:
 */
public class Jobest {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setJarByClass(Jobest.class);

        job.setMapperClass(MAPERtest.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setReducerClass(REDUCEtest.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        File output = new File("f:\\output");
        if (output.exists()){
            FileUtil.fullyDelete(output);
        }

        FileInputFormat.setInputPaths(job,new Path("f:\\input"));
        FileOutputFormat.setOutputPath(job,new Path("f:\\output"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
