package com.etc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/8/31 15:01
 * @Description:
 */
public class REDUCEtest extends Reducer<Text,IntWritable,Text,IntWritable> {

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int count =0;
        int i = 0;
        for (IntWritable value : values) {
            count += value.get();
            i++;
        }
        context.write(key,new IntWritable(count/i));
    }
}
