package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.Properties;

public class JobSubmitter {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        Properties properties = new Properties();
        properties.load(JobSubmitter.class.getClassLoader().getResourceAsStream("topn.properties"));

        Configuration conf = new Configuration();

        //conf.setInt("top.n",Integer.parseInt(properties.getProperty("top.n")));
        //conf.setInt("top.n",Integer.parseInt(args[0]));
        conf.setInt("top.n",3);
        Job job = Job.getInstance(conf);

        job.setJarByClass(JobSubmitter.class);

        job.setMapperClass(PageTopnMapper.class);
        job.setReducerClass(PageTopnReduce.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);


        FileInputFormat.setInputPaths(job,new Path("C:\\Users\\Administrator\\Desktop\\大数据讲义\\第三阶段Hadoop\\mapreduce练习\\练习二页面访问次数\\练习用的数据\\input"));
        FileOutputFormat.setOutputPath(job,new Path("C:\\Users\\Administrator\\Desktop\\大数据讲义\\第三阶段Hadoop\\mapreduce练习\\练习二页面访问次数\\练习用的数据\\output"));


        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
