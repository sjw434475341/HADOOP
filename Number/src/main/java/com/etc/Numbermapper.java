package com.etc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 18:51
 * @Description:
 */
public class Numbermapper extends Mapper<LongWritable,Text,IntWritable,NullWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        int num = Integer.parseInt(value.toString());
        context.write(new IntWritable(num), NullWritable.get());
    }
}
