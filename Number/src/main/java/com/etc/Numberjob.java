package com.etc;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;


/**
 * @Auther: Administrator
 * @Date: 2018/9/3 19:20
 * @Description:
 */
public class Numberjob {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);

        Job job =Job.getInstance(conf);
        job.setJarByClass(Numberjob.class);
        job.setMapperClass(Numbermapper.class);
        job.setReducerClass(Numberreduce.class);

        job.setMapOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(NullWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        Path inputPath = new Path("d:/input/");//读入多个文件
        Path outputPath = new Path("d:/output/");//输出一个文件
        if (fs.exists(inputPath)) {
            fs.delete(outputPath, true);
        }

        FileInputFormat.setInputPaths(job, inputPath);
        FileOutputFormat.setOutputPath(job, outputPath);
        boolean isdone = job.waitForCompletion(true);
        System.exit(isdone ? 0 : 1);
    }


}
