package com.etc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


/**
 * @Auther: Administrator
 * @Date: 2018/9/3 18:58
 * @Description:
 */
public class Numberreduce extends Reducer<IntWritable,NullWritable,Text,NullWritable> {
    Text t = new Text();
    int tnum = 0;
    int cnum = 0;

    @Override
    protected void reduce(IntWritable key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
        for(NullWritable niv : values){
            if (key.get() > tnum) {
                cnum++;//全局排序变量
                tnum = key.get();//记录当前临时值
            }
            String kk = cnum + "\t" + key.toString();
            t.set(kk);
            context.write(t, NullWritable.get());
        }
    }

}

