package com.etw;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MapR extends Mapper<LongWritable,Text,Text, Information> {

    public static Text transformTextToUTF8(Text text, String encoding) {
        String value = null;
        try {
            value = new String(text.getBytes(), 0, text.getLength(), encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new Text(value);
    }
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        Text newText = transformTextToUTF8(value, "GBK");
         String  a= newText.toString();
        String[]  word =a.split(",");
        String user =word[1];
        String name =word[2];
        double price =Double.parseDouble(word[3]);
        int number =Integer.parseInt(word[4]);
        double s =Integer.parseInt(word[4])*Double.parseDouble(word[3]);
        context.write(new Text(word[0]),new Information(user,name,price ,number,s));
    }
}
