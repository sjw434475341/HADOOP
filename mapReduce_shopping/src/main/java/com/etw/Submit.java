package com.etw;



import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;


public class Submit {
    public static void main(String[] args)throws Exception {
        Configuration conf=new Configuration();

        Job job = Job.getInstance(conf);

        job.setJarByClass(Submit.class);
        job.setMapperClass(MapR.class);
        job.setReducerClass(ReduceR.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Information.class);

        job.setOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        File file =new File("f:/words/outputc");
        if(file.exists()){
            FileUtils.deleteDirectory(file);
        }

        FileInputFormat.setInputPaths(job,new Path("f:/words/inputc"));
        FileOutputFormat.setOutputPath(job,new Path("f:/words/outputc"));

        job.setNumReduceTasks(2);

        System.out.println(job.waitForCompletion(true)?0:1);
    }
}
