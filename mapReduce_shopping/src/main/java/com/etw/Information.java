package com.etw;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Information implements Comparable<Information>, Writable {


    private String user;
    private String productName;
    private double price;
    private int  numbers;
    private double sum;


    public String getUser() { return user; }
    public void setUser(String user) { this.user = user; }
    public String getProductName() { return productName; }
    public void setProductName(String productName) { this.productName = productName; }
    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }
    public int getNumbers() { return numbers; }
    public void setNumbers(int numbers) { this.numbers = numbers; }
    public double getSum() { return sum; }
    public void setSum(double sum) { this.sum = sum; }

    public Information(String user,String productName ,double price ,int numbers,double sum){
        this.user=user;this.productName=productName;this.price =price;this.numbers=numbers;this.sum=sum;
    }


    public Information(){}

    @Override
    public int compareTo(Information o) {
        return o.getSum()-this.getSum()==0?this.productName.compareTo(o.getProductName()):(int)(o.getSum()-this.getSum());
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
       dataOutput.writeUTF(this.user);
       dataOutput.writeUTF(this.productName);
       dataOutput.writeDouble(this.price);
       dataOutput.writeInt(this.numbers);
       dataOutput.writeDouble(this.sum);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.user = dataInput.readUTF();
        this.productName =dataInput.readUTF();
        this.price= dataInput.readDouble();
        this.numbers = dataInput.readInt();
        this.sum = dataInput.readDouble();


    }
}
