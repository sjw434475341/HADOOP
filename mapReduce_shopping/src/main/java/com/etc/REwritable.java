package com.etc;


import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 14:59
 * @Description:
 */
public class REwritable implements WritableComparable<REwritable> {

    String name;
    long  num;
    float price;
    float sumprice;

    public REwritable() {
    }

    public REwritable(long num, float  price) {
        this.num = num;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getSumprice() {
        return sumprice;
    }

    public void setSumprice() {
        this.sumprice = price*num;
    }

    @Override
    public void write(DataOutput out) throws IOException {
       out.writeLong(num);
       out.writeFloat(price);
       out.writeFloat(sumprice);
    }

    @Override
    public String toString() {
        return  num + "," + price + "," + sumprice;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
          num = in.readLong();
          price = in.readFloat();
          sumprice = in.readFloat();
    }

    public void set(long num, float price) {
        this.num = num;
        this.price = price;
        this.sumprice = num + price;
    }

    @Override
    public int compareTo(REwritable o) {

            return this.sumprice > o.getSumprice() ? -1 : 1;

    }
}
