package com.etc;


import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 14:56
 * @Description:
 */
public class mappershop extends Mapper<LongWritable,Text,Text,REwritable> {

    REwritable  rw = new REwritable();
    Text t = new Text();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //获取第一行数据
        String str = transformTextToUTF8(value,"GBK").toString();
        //切分
        String[] line = str.split(",");
        //封装对象
        String name = line[2];
        long num = Long.parseLong(line[line.length-1]);
        float price = Float.parseFloat(line[line.length-2]);

        rw.set(num,price);
        t.set(name);

       context.write(t,rw);
    }
    public static Text transformTextToUTF8(Text text, String encoding) {
        String value = null;
        try {
            value = new String(text.getBytes(), 0, text.getLength(), encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new Text(value);
    }
}
