package com.etc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 15:28
 * @Description:
 */
public class reduceshop extends Reducer<Text,REwritable,Text,REwritable> {
    List<REwritable> list = new ArrayList<>();

    @Override
    protected void reduce(Text key, Iterable<REwritable> values, Context context) throws IOException, InterruptedException {
        REwritable result = new REwritable();
        int a = 0 ,b = 0;
        for (REwritable value : values){
            a+=value.getNum();
            b+=value.getPrice();
        }

        result.setName(key.toString());
        result.setNum(a);
        result.setPrice(b);
        result.setSumprice();
        list.add(result);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        Collections.sort(list);
        for (int i=0 ; i<3 ; i++){
            context.write(new Text(list.get(i).getName()),list.get(i));
        }
    }
}
