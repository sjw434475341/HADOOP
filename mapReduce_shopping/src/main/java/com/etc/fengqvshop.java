package com.etc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

import java.util.HashMap;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 17:44
 * @Description:
 */
public class fengqvshop extends Partitioner<Text,REwritable> {

    static HashMap<String, Integer> provinceMap = new HashMap<>();
    static {
        provinceMap.put("order001",1);
        provinceMap.put("order002",2);
    }

    @Override
    public int getPartition(Text text, REwritable rEwritable, int i) {
        Integer code = provinceMap.get(text.toString().substring(0,4));
        return code == null ? 2:code;
    }
}
