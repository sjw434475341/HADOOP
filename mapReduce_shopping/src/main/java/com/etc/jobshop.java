package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/3 15:38
 * @Description:
 */
public class jobshop {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //获取对象信息或job对象实例
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        //指定本程序的jar包所在的本地路径
        job.setJarByClass(jobshop.class);

        //指定本业务job要使用的mapper/reduceer服务
        job.setMapperClass(mappershop.class);
        job.setReducerClass(reduceshop.class);

        //指定mapper输出数据的kv类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(REwritable.class);

        //指定最终输出数据的kv类型
        job.setOutputKeyClass(REwritable.class);
        job.setOutputValueClass(Text.class);

        File output = new File("e:\\output");
        if (output.exists()){
            FileUtil.fullyDelete(output);
        }

        //指定job的输入原始目录所在
        FileInputFormat.setInputPaths(job , new Path("e:\\input"));
        FileOutputFormat.setOutputPath(job , new Path("e:\\output"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
