package etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @Auther: Administrator
 * @Date: 2018/8/30 14:51
 * @Description:
 */
public class HdfsJob {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, URISyntaxException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setJarByClass(HdfsJob.class);

        job.setMapperClass(HdfsMAPPER.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setReducerClass(HdfsREDUCE.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileSystem fs = FileSystem.get(new URI("hdfs://192.168.33.2:9000/"), conf, "root");
        if (fs.exists(new Path("/output"))){
            fs.delete(new Path("/output"),true);
        }
        FileInputFormat.setInputPaths(job,new Path("hdfs://192.168.33.2:9000/input"));
        FileOutputFormat.setOutputPath(job,new Path("hdfs://192.168.33.2:9000/output"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
