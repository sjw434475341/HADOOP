package etc;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


/**
 * @Auther: Administrator
 * @Date: 2018/8/30 14:24
 * @Description:
 */
public class HdfsMAPPER extends Mapper<LongWritable,Text,Text,IntWritable> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String linex = value.toString();
        String[] split = linex.split(",");
        for (String s: split){
            context.write(new Text(s) , new IntWritable(1));
        }
    }
}
