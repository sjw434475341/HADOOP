package com.etc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


import java.io.IOException;


/**
 * @Auther: Administrator
 * @Date: 2018/9/6 10:07
 * @Description:
 */
public class Doublereducetwo extends Reducer<Text,Text,Text,Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        StringBuffer sb = new StringBuffer();

        for (Text value : values) {
             sb.append(value.toString().replace( ""," "));
        }

        context.write(key,new Text(sb.toString()));


    }
}
