package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/6 09:27
 * @Description:
 */
public class jobone {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setJarByClass(jobone.class);

        job.setMapperClass(Doublemapperone.class);
        job.setReducerClass(Doublereduceone.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        File output = new File("f:\\double\\output1");
        if (output.exists()){
            FileUtil.fullyDelete(output);
        }

        FileInputFormat.setInputPaths(job,new Path("f:\\double\\input"));
        FileOutputFormat.setOutputPath(job,new Path("f:\\double\\output1"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
