package com.etc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: Administrator
 * @Date: 2018/9/6 09:23
 * @Description:
 */
public class Doublereduceone extends Reducer<Text,Text,Text,Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        List<Text> sb = new ArrayList<>();
        for (Text x : values){

            sb.add(new Text(x));
        }

        for (int i=0 ; i<sb.size(); i++){
            for (int j = 0; j<sb.size(); j++){
                if(sb.get(i)!=sb.get(j)) {
                    context.write(new Text(sb.get(i) + "-" + sb.get(j)), key);
                }
            }

        }

    }
}
