package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/8/31 17:20
 * @Description:
 */
public class chongxieJob {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setJarByClass(chongxieJob.class);

        job.setMapperClass(qvchongMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setReducerClass(qvchongReduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        File output = new File("f:\\qvchong\\output");
        if (output.exists()){
            FileUtil.fullyDelete(output);
        }

        FileInputFormat.setInputPaths(job,new Path("f:\\qvchong\\input"));
        FileOutputFormat.setOutputPath(job,new Path("f:\\qvchong\\output"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
