package com.etc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/8/31 16:14
 * @Description:
 */
public class qvchongMapper extends Mapper<LongWritable,Text,Text,IntWritable> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
       //获取第一行数据
        String str = value.toString();
       //切开
        //String[] split = str.split(" ");
       //提取KEY和VALUES
        context.write(new Text(str),new IntWritable());
    }
}
