package com.etc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/4 17:45
 * @Description:
 */
public class jobJIEHE {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setJarByClass(jobJIEHE.class);

        job.setMapperClass(mapperJIEHE.class);
        job.setReducerClass(reducerJIEHE.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(REwritable.class);

        job.setOutputKeyClass(REwritable.class);
        job.setOutputValueClass(NullWritable.class);

        File output = new File("d:\\add\\output");
        if (output.exists()){
            FileUtil.fullyDelete(output);
        }

        //指定job的输入原始目录所在
        FileInputFormat.setInputPaths(job , new Path("d:\\add\\input"));
        FileOutputFormat.setOutputPath(job , new Path("d:\\add\\output"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);

    }


}
