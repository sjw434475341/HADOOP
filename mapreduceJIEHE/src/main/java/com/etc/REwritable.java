package com.etc;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/4 16:08
 * @Description:
 */
public class REwritable extends Text implements Writable {
    private String orderid;
    private String userid;
    private String name;
    private int age;
    private String name2;

    public REwritable() {
    }

    public REwritable(String userid, String name, int age, String name2) {
        this.userid = userid;
        this.name = name;
        this.age = age;
        this.name2 = name2;
    }

    public REwritable(String orderid, String userid, String name, int age, String name2) {
        this.orderid = orderid;
        this.userid = userid;
        this.name = name;
        this.age = age;
        this.name2 = name2;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(orderid);
        out.writeUTF(userid);
        out.writeUTF(name);
        out.writeUTF(name2);
        out.writeInt(age);
    }

    @Override
    public String toString() {
        return orderid + "\t" + name + "\t" + age +"\t";
    }

    @Override
    public void readFields(DataInput in) throws IOException {
          orderid = in.readUTF();
          userid = in.readUTF();
          name = in.readUTF();
          name2 = in.readUTF();
          age = in.readInt();
    }


}
