package com.etc;


import org.apache.commons.beanutils.BeanUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: Administrator
 * @Date: 2018/9/4 17:16
 * @Description:
 */
public class reducerJIEHE extends Reducer<Text,REwritable,Text,NullWritable> {

    @Override
    protected void reduce(Text key, Iterable<REwritable> values, Context context) throws IOException, InterruptedException {
        List<REwritable> list =new ArrayList<>();

        REwritable rw = new REwritable();

        for (REwritable re : values){
            if ("0".equals(rw.getAge())){
                REwritable rewr = new REwritable();
                try {
                    BeanUtils.copyProperties(rewr,re);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                list.add(rewr);
            }else {
                try {
                    BeanUtils.copyProperties(rw,re);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
            for (REwritable rewri : list){
                rewri.setUserid(rw.getUserid());
                context.write(rewri,NullWritable.get());
        }

    }
}
