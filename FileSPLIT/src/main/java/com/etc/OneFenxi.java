package com.etc;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import java.io.File;
import java.io.IOException;

/**
 * @Auther: Administrator
 * @Date: 2018/9/4 08:56
 * @Description:
 */
public class OneFenxi  {

    public static class OneMapper extends Mapper<LongWritable,Text,Text,IntWritable>{

        private String fileName = "";

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            FileSplit fileSplit = (FileSplit) context.getInputSplit();
            fileName = fileSplit.getPath().getName();
            System.out.println(fileName);
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
               String line = value.toString();
               String[] split = line.split(" ");
               for (String s : split){
                   context.write(new Text(s+"-"+fileName),new IntWritable(1));
               }
        }
    }

    public static class OneReduce extends Reducer<Text,IntWritable,Text,IntWritable> {

        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int count = 0;
            for (IntWritable t : values){
                count += t.get();
            }
            context.write(key,new IntWritable(count));
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration  conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setJarByClass(OneFenxi.class);

        job.setMapperClass(OneFenxi.OneMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setReducerClass(OneFenxi.OneReduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        File output =  new File("D:\\练习数据\\output");
        if (output.exists()){
            FileUtil.fullyDelete(output);
        }

        FileInputFormat.setInputPaths(job,new Path("D:\\练习数据\\input"));
        FileOutputFormat.setOutputPath(job,new Path("D:\\练习数据\\output"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b?1:-1);
    }
}
