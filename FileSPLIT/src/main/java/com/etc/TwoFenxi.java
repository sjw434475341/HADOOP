package com.etc;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;


/**
 * @Auther: Administrator
 * @Date: 2018/9/4 09:58
 * @Description:
 */
public class TwoFenxi {

    public static class TwoMapper extends Mapper<LongWritable, Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String str = value.toString();
            String[] split = str.split("-");
            context.write(new Text(split[0]), new Text(split[1]));
        }
    }

    public static class TwoReduce extends Reducer<Text, Text, Text, Text> {

        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
             StringBuilder sb = new StringBuilder();

            for (Text value : values){
                sb.append(value.toString().replace("\t","-->") + "\t");
            }
                context.write(key,new Text(sb.toString()));
            }

        }



    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);

        job.setJarByClass(TwoFenxi.class);

        job.setMapperClass(TwoFenxi.TwoMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setReducerClass(TwoFenxi.TwoReduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        File output = new File("D:\\练习数据\\output1");
        if (output.exists()) {
            FileUtil.fullyDelete(output);
        }

        FileInputFormat.setInputPaths(job, new Path("D:\\练习数据\\output"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\练习数据\\output1"));

        boolean b = job.waitForCompletion(true);
        System.out.println(b ? 1 : -1);
    }
}